from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps

from .apps import BooksConfig
from .views import book
# Create your tests here.

class BooksUnitTest(TestCase):

    def test_books_app(self):
        self.assertEqual(BooksConfig.name, 'books')
        self.assertEqual(apps.get_app_config('books').name, 'books')

    def test_landing_page_url_books_exist(self):
        response = self.client.get('/books')
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, 404)

    def test_landing_page_using_book_function(self):
        response = resolve('/books')
        self.assertEqual(response.func, book)

    def test_landing_page_using_book_template(self):
        response = self.client.get('/books')
        self.assertTemplateUsed(response, 'book.html')
        self.assertContains(response, 'Book Finder')