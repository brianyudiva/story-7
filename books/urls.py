from django.urls import path
from .views import book

app_name = 'books'

urlpatterns = [
    path('', book, name='books'),
]