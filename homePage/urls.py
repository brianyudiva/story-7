from django.urls import path
from .views import index

app_name = 'homePage'

urlpatterns = [
    path('', index, name='homepage'),
]