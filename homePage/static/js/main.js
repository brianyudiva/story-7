$(document).ready(function () {

    $('.title').siblings().hide()

    $('.title').on('click', function() {
        if (!$(this).siblings().hasClass("shown")){
            // console.log("masuk if")
            $('.title').siblings().slideUp()
            $('.title').siblings().removeClass("shown")
            $(this).siblings().slideDown()
            $(this).siblings().addClass("shown")
        }else{
            $('.title').siblings().removeClass("shown")
            $('.title').siblings().slideUp()
        }
            // console.log("masuk else")
            
        
    });

    // ============================================================================================


    $('input[type=checkbox]').change(function(){
        var changeableElement = ["a", "h1", "p", "td", "th", 'li', 'svg']
        if ($(this).is(':checked')) {
            for(i=0;i<changeableElement.length;i++){
                $(changeableElement[i]).addClass("text-light");
            }
            $('body').removeClass("bg-light").addClass("bg-dark");
            $('.title').removeClass('bg-dark').addClass("bg-light");
            $('.icon16').addClass("fill-white");
            $('.icon24').addClass("fill-white");
            $('.ac-i').removeClass('text-light').addClass('text-dark');
            $('.ac-s').removeClass('fill-white');
            $('.vr').addClass("vr-white");
        }else{
            for(i=0;i<changeableElement.length;i++){
                $(changeableElement[i]).removeClass("text-light");
            }
            $('body').removeClass("bg-dark").addClass("bg-light");
            $('.title').removeClass("bg-light").addClass('bg-dark');
            $('.icon16').removeClass("fill-white");
            $('.icon24').removeClass("fill-white");
            $('.ac-i').removeClass('text-dark').addClass('text-light');
            $('.ac-s').addClass('fill-white');
            $('.vr').removeClass("vr-white");
        }
    });
});
