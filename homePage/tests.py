from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps

from .apps import HomepageConfig
from .views import index

# Create your tests here.

class HomePageUnitTest(TestCase):

    def test_story7_app(self):
        self.assertEqual(HomepageConfig.name, 'homePage')
        self.assertEqual(apps.get_app_config('homePage').name, 'homePage')

    def test_landing_page_url_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, 404)

    def test_landing_page_using_index_function(self):
        response = resolve('/')
        self.assertEqual(response.func, index)

    def test_landing_page_using_index_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')
        self.assertContains(response, 'Brian Athallah Yudiva')