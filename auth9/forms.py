from django import forms

class UserForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'id' : 'user',
        'placeholder' : 'Enter your username here'
    }))

    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class' : 'form-control',
        'id' : 'passw',
        'placeholder' : 'Password'
    }))