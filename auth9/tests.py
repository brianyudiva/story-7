from django.test import TestCase, Client, RequestFactory
from django.contrib.auth.models import User
from django.urls import resolve
from django.apps import apps
from django.contrib import auth

from .apps import Auth9Config
from .views import register, loginUser, logoutUser
from .forms import UserForm
# Create your tests here.

class Auth9UnitTest(TestCase):
    def setUp(self):
        super().setUp()
        self.user = User.objects.create_user(username='jacob', password='top_secret')
        session = self.client.session

    def test_auth_app(self):
        self.assertEqual(Auth9Config.name, 'auth9')
        self.assertEqual(apps.get_app_config('auth9').name, 'auth9')
    
    def test_url_login(self):
        response = self.client.get('/login')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

    def test_url_register(self):
        response = self.client.get('/register')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'register.html')

    def test_url_register_using_register_function(self):
        response = resolve('/register')
        self.assertEqual(response.func, register)

    def test_url_login_using_login_function(self):
        response = resolve('/login')
        self.assertEqual(response.func, loginUser)

    def test_user_logged_in_success(self):
        data = {
            'username' : 'jacob',
            'password' : 'top_secret'
        }

        form = UserForm(data = data)
        self.assertTrue(form.is_valid())
        self.client.login(username = form.cleaned_data['username'], password = form.cleaned_data['password'])
        self.assertTrue(self.client.session['_auth_user_id'] is not None)

    def test_user_logged_in_failed(self):
        data = {
            'username' : 'jacob',
        }
        form = UserForm(data=data)
        self.assertFalse(form.is_valid())

    def test_user_logged_out(self):
        self.client.login(username='jacob',password='top_secret')
        self.assertTrue(self.client.session['_auth_user_id'] is not None)
        self.client.get('/logout')
        self.assertEqual(len(self.client.session.keys()),0)

    # def test_user_logged_in(self):
    #     # response = self.client.post('/login', {'username' : 'jacob', 'password' : 'top_secret'})
    #     # user = User.objects.get_by_natural_key(username='jacob')
    #     self.assertIn('_auth_user_id', self.client.session)

        